window.headlineCreator = (function(){
    function createHeadline(text, parentEle='#root') {
        const parentElementSelector = document.querySelector(parentEle);
        const headLine = document.createElement('h1');

        headLine.innerText = text;
        parentElementSelector.appendChild(headLine);
        return headLine;
    }

    return {
        createHeadline : createHeadline
    };
}) ();